#include <fcntl.h>
#include <iostream>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

#define MAX_BUFFER 1024

using namespace std;

int main(void)
{
	int fifo;
	char msj[MAX_BUFFER];
	
	const char *namepipe = "/tmp/pipe";
	fifo = mkfifo(namepipe, 0666);
	if(fifo == -1)
	{
		perror("Error al crear el fifo");
		return 0;
	} 
	
	int pipe = open(namepipe, O_RDONLY);
	
	if(pipe == -1)
	{
		perror("Error al abrir el fifo");
		exit(1);
	}
	else
	{
		while(1)
		{
			if(read(pipe, msj, sizeof(msj)) > 0)
				cout << msj << endl;
			else
				break;
		}
	}
	
	close(pipe);
	unlink(namepipe);
	
	return 0;
}
