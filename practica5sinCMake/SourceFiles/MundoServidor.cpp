// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "../HeaderFiles/MundoServidor.h"
#include "../HeaderFiles/glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

void* hilo_comandos(void* d)
{
      CMundo* p=(CMundo*) d;
      p->RecibeComandosJugador();
}

void CMundo::RecibeComandosJugador()
{
     while (1) {
            usleep(10);
            char cad[100];
//			read(fdtecla, cad, sizeof(cad));
			socketjugador1.Receive(cad, sizeof(cad));
            unsigned char key;
            sscanf(cad,"%c",&key);
			printf("%c",key);
            if(key=='q')jugador1.DisminuyeVelocidad(0);
            if(key=='e')jugador1.AumentaVelocidad(0);
            if(key=='s')/*jugador1.velocidad.y=-4;*/jugador1.DisminuyeVelocidad(1);
            if(key=='w')/*jugador1.velocidad.y=4;*/jugador1.AumentaVelocidad(1);
            if(key=='l')/*jugador2.velocidad.y=-4;*/
            {
				jugador2.DisminuyeVelocidad(1);
				DesactivaBot();
			}
            if(key=='o')/*jugador2.velocidad.y=4;*/
            {
				jugador2.AumentaVelocidad(1);
				DesactivaBot();
			}
            if(key=='i')
            {
				jugador2.DisminuyeVelocidad(0);
				DesactivaBot();
			}
            if(key=='p')
            {
				jugador2.AumentaVelocidad(0);
				DesactivaBot();
			}
            if(key=='d')CreaDisparo(jugador1.centro, 4.0f, 1);
            if(key=='k')
            {
				CreaDisparo(jugador2.centro, -4.0f, 2);
				DesactivaBot();
			}
            if(key=='h')pausa = not(pausa);
      }
}

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
	//Cierre del fifo
	int clogger = close(loggerfifo);
	if(clogger==-1)
	{
		perror("Error: Cerrar tuberia logger en Mundo");
	}

 	/*//Cierre de la proyección de memoria
	munmap(proyeccion,sizeof(Datos));*/
	
	//cerrar el fifo de comunicacion entre cliente y servidor para coordenadas
	
//	int cclsrvcoor = close(fdcoordenadas);
//	if(cclsrvcoor==-1)
//	{
//		perror("Error: Cerrar tuberia CS en Mundo");
//	}

	//cerrar el fifo de comunicacion entre cliente y servidor para teclas
	
//	int cclsrvte = close(fdtecla);
//	if(cclsrvte==-1)
//	{
//		perror("Error: Cerrar tuberia CS en Mundo cliente para teclas");
//	}
	
//	int cclsrvesf[MAX_ESFERAS];
//	cclsrvesf[0] = close(fdesferas[0]);
//	if(cclsrvesf[0]==-1)
//	{
//		perror("Error: Cerrar tuberia CS en Mundo cliente para esfera0");
//	}

//	for(int i=0; i<MAX_ESFERAS; i++)
//	{
//		cclsrvesf[i] = close(fdesferas[i]);
//		if(cclsrvesf[i]==-1)
//		{
//			char cad[100];
//			sprintf(cad,"Error: Cerrar tuberia CS en Mundo Servidor para esfera%d",i);
//			perror(cad);			
//		}
//	}
	
//	int ccsdisp1 = close(fddisparo1);
//	if(ccsdisp1==-1)
//	{
//		perror("Error: Cerrar tuberia CS en Mundo cliente para disparo1");
//	}
	
//	int ccsdisp2 = close(fddisparo2);
//	if(ccsdisp2==-1)
//	{
//		perror("Error: Cerrar tuberia CS en Mundo cliente para disparo2");
//	}
	
//	int ccscrono = close(fdcrono);
//	if(ccscrono==-1)
//	{
//		perror("Error: Cerrar tuberia CS en Mundo cliente para crono");
//	}
	
//	int ccsIA = close(fdIA);
//	if(fdIA==-1)
//	{
//		perror("Error: Cerrar tuberia CS en Mundo cliente para IA");
//	}
		
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	
	char cad[100];
	sprintf(cad,"Jugador1 %s: %d",nombrejugador1, puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	
	char cadcrono[100];
	sprintf(cadcrono,"%f",contador);
	print(cadcrono,400,0,1,1,1);
	
	/////////// MODO PAUSA ////////
	char cadpausa[] = "PAUSE (Pulse H para continuar)";
	if(pausa == true)
	{
		print(cadpausa,300,300,1,1,1);
    }
    
    /////////// MODO BOT //////////
    char cadBot[] = "BOT activado";
    if(botActivo == 1)
    {
		print(cadBot,400,0,0,255,0);
	}

	int i,j;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();
	
	for(i=0; i<nEsferas; i++)
	{
		esferas[i]->Dibuja();
	}
	
	if(disparo1A == 1) disparo1->Dibuja();
	if(disparo2A == 1) disparo2->Dibuja();
	

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{
/*	char cadcrono[50];
	if(botActivo == 0)
	{
		read (fdcrono,cadcrono,sizeof(cadcrono));
		socketservidor.Receive(cadcrono,sizeof(cadcrono));
		
		sscanf(cadcrono,"%f",&contador);
	}
	if(botActivo == 1)
	{
		read (fdcrono,cadcrono,sizeof(cadcrono));
		socketservidor.Receive(cadcrono,sizeof(cadcrono));
	
		sscanf(cadcrono,"%f %d",&contador,&IA);		
	}*/
	//
		
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	
	jugador1.SetCentro();
	jugador2.SetCentro();
	
	esfera.Mueve(0.025f);
	
	int i;
	int j;
	
	for(i=0; i<nEsferas; i++)
	{
		esferas[i]->Mueve(0.025f);
	}
	
	if(disparo1A == 1) disparo1->Mueve(0.025f);
	if(disparo2A == 1) disparo2->Mueve(0.025f);
	
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}
	
	for(i=0;i<paredes.size();i++)
	{
		for(j=0; j<nEsferas; j++)
		{
			paredes[i].Rebota(*(esferas[j]));
		}
	}
	
	if(disparo1A == 1)
	{
		for(i=0;i<paredes.size();i++)
		{
			paredes[i].Rebota(*(disparo1));
		}
	}
	
	if(disparo2A == 1)
	{
		for(i=0;i<paredes.size();i++)
		{
			paredes[i].Rebota(*(disparo2));
		}
	}

//	jugador1.Rebota(esfera);
//	jugador2.Rebota(esfera);

	if(jugador1.Rebota(esfera))nRebotes++;
	if(jugador2.Rebota(esfera))nRebotes++;
	
	for(i=0; i<nEsferas; i++)
	{
		if(jugador1.Rebota(*(esferas[i])))nRebotes++;
		if(jugador2.Rebota(*(esferas[i])))nRebotes++;
	}


	if(fondo_izq.Rebota(esfera))
	{
		DestruyeEsferas();
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		
		sprintf(cadfifo, "Jugador 2 anota un punto, lleva %d puntos", puntos2);
			
		if(write(loggerfifo, cadfifo, (strlen(cadfifo)+1)*sizeof(char)) == -1)
		{
			perror("Error en la escritura");
			exit(1);
		}
		
	}

	if(fondo_dcho.Rebota(esfera))
	{
		DestruyeEsferas();
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
		
		sprintf(cadfifo, "Jugador 1 anota un punto, lleva %d puntos", puntos1);
			
		if(write(loggerfifo, cadfifo, (strlen(cadfifo)+1)*sizeof(char)) == -1)
		{
			perror("Error en la escritura");
			exit(1);
		}
				
	}
	
	for(i=0; i<nEsferas; i++)
	{
		if(fondo_izq.Rebota(*(esferas[i])))
		{
			DestruyeEsferas();
			esfera.centro.x=0;
			esfera.centro.y=rand()/(float)RAND_MAX;
			esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
			esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
			puntos2++;
			
			sprintf(cadfifo, "Jugador 2 anota un punto, lleva %d puntos", puntos2);
			
			if(write(loggerfifo, cadfifo, (strlen(cadfifo)+1)*sizeof(char)) == -1)
			{
				perror("Error en la escritura");
				exit(1);
			}

		}

		if(fondo_dcho.Rebota(*(esferas[i])))
		{
			DestruyeEsferas();
			esfera.centro.x=0;
			esfera.centro.y=rand()/(float)RAND_MAX;
			esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
			esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
			puntos1++;
			
			sprintf(cadfifo, "Jugador 1 anota un punto, lleva %d puntos", puntos1);
			
			if(write(loggerfifo, cadfifo, (strlen(cadfifo)+1)*sizeof(char)) == -1)
			{
				perror("Error en la escritura");
				exit(1);
			}

		}	
	}
	
	if(disparo1A == 1)
	{
		if(fondo_dcho.Rebota(*(disparo1)))
		{
			delete disparo1;
			disparo1A = 0;
			puntos1++;
			
			sprintf(cadfifo, "Jugador 2 no ha podido detener disparo, Jugador 1 consigue un punto, lleva %d puntos", puntos1);
			
			if(write(loggerfifo, cadfifo, (strlen(cadfifo)+1)*sizeof(char)) == -1)
			{
				perror("Error en la escritura");
				exit(1);
			}
		}
		
		if(jugador2.Rebota(*(disparo1)))
		{
			delete disparo1;
			disparo1A = 0;
			puntos2++;
			
			sprintf(cadfifo, "Jugador 2 ha detenido el disparo, Jugador 2 consigue un punto, lleva %d puntos", puntos2);
			
			if(write(loggerfifo, cadfifo, (strlen(cadfifo)+1)*sizeof(char)) == -1)
			{
				perror("Error en la escritura");
				exit(1);
			}
		}
	}
	
	if(disparo2A == 1)
	{
		if(fondo_izq.Rebota(*(disparo2)))
		{
			delete disparo2;
			disparo2A = 0;
			puntos2++;
			
			sprintf(cadfifo, "Jugador 1 no ha podido detener disparo, Jugador 2 consigue un punto, lleva %d puntos", puntos2);
			
			if(write(loggerfifo, cadfifo, (strlen(cadfifo)+1)*sizeof(char)) == -1)
			{
				perror("Error en la escritura");
				exit(1);
			}
		}
		
		if(jugador1.Rebota(*(disparo2)))
		{
			delete disparo2;
			disparo2A = 0;
			puntos1++;
			
			sprintf(cadfifo, "Jugador 1 ha detenido el disparo, Jugador 1 consigue un punto, lleva %d puntos", puntos1);
			
			if(write(loggerfifo, cadfifo, (strlen(cadfifo)+1)*sizeof(char)) == -1)
			{
				perror("Error en la escritura");
				exit(1);
			}
		}
	}
	
	
	if(nRebotes >= 3) 
	{
		CreaEsferas();
		nRebotes = 0;
		
		sprintf(cadfifo, "Esfera creada, hay %d esferas", nEsferas+1);
		
		if(write(loggerfifo, cadfifo, (strlen(cadfifo)+1)*sizeof(char)) == -1)
		{
			perror("Error en la escritura");
			exit(1);
		}
	}
	
	for(i=0; i<nEsferas; i++)
	{
		esfera.Rebota(esferas[i]);
	}
	
	for(i=0; i<nEsferas; i++)
	{
		for(j=i+1; j<nEsferas; j++)
		{
			esferas[i]->Rebota(esferas[j]);
		}
	}
	
	if(disparo1A == 1 && (esfera.Rebota(disparo1)))
	{
		delete disparo1;
		disparo1A = 0;
	}
	if(disparo2A == 1 && (esfera.Rebota(disparo2)))
	{
		delete disparo2;
		disparo2A = 0;
	}

	for(i=0; i<nEsferas; i++)
	{
		if(disparo1A == 1 && (esferas[i]->Rebota(disparo1)))
		{
			delete disparo1;
			disparo1A = 0;
		}
		if(disparo2A == 1 && (esferas[i]->Rebota(disparo2)))
		{
			delete disparo2;
			disparo2A = 0;
		}
	}
	
	//ENCAPSULAMIENTO RAQUETAS//
	
	//Jugador1
	
	if((jugador1.x1 <= -7) || (jugador1.x1 >= 7))
	{
		jugador1.g=0;
		jugador1.x1=-6;jugador1.y1=-1;
		jugador1.x2=-6;jugador1.y2=1;
		jugador1.velocidad.x = 0;
		jugador1.velocidad.y = 0;
	}
	
	//Jugador2
	
	if((jugador2.x1 <= -7) || (jugador2.x1 >= 7))
	{
		jugador2.g=0;
		jugador2.x1=6;jugador2.y1=-1;
		jugador2.x2=6;jugador2.y2=1;
		jugador2.velocidad.x = 0;
		jugador2.velocidad.y = 0;
	}
	
	//LOGGER PAUSE Todavia en pruebas
	
/*	if(pausa == true)
	{
		sprintf(cadfifo, "Modo pausa activado");
		
		if(write(loggerfifo, cadfifo, (strlen(cadfifo)+1)*sizeof(char)) == -1)
		{
			perror("Error en la escritura");
			exit(1);
		}		
	}*/
	
/*	if(botActivo == true)
	{
		switch(pMemCompartida->accion)
		{
			case 1:
				OnKeyboardDown('l', 0, 0);
				botActivo = true;
				break;
			case 0:
				botActivo = true;
				break;
			case -1:
				OnKeyboardDown('o', 0, 0);
				botActivo = true;
				break;
			case 2:
				OnKeyboardDown('k', 0, 0);
				botActivo = true;
				break;
			default:
				break;
		}
	}
	
	//Comprobamos que esfera es la mas cercana
	
	for(int i=0; i<nEsferas; i++)
	{
		if(esferas[i]->centro.x > lamasCercana->centro.x)
			lamasCercana = esferas[i];
	}
	
	if(esfera.centro.x > lamasCercana->centro.x)
		lamasCercana = &esfera;
		
	if((disparo1A == true) && (disparo1->centro.x > lamasCercana->centro.x))
		lamasCercana = disparo1;
	
	//
	
	pMemCompartida->esfera = *lamasCercana;
	pMemCompartida->raqueta1 = jugador2;
	pMemCompartida->disparo2A = this->disparo2A;
	
	contador+=0.025f;
*/	
	if(contador >= 15.0f)
		botActivo = 1;
		
		
	//Salida del juego si un jugador gana la partida por mas de 7 puntos
	//Jugador1
	if(puntos1 >= 11)		//Susceptible a cambio para testing
	{
		sprintf(cadfifo, "Enhorabuena jugador1, has ganado la partida");
		
		if(write(loggerfifo, cadfifo, (strlen(cadfifo)+1)*sizeof(char)) == -1)
		{
			perror("Error en la escritura");
			exit(1);
		}
		
//		unlink("/tmp/fifoCS");
		
//		unlink("/tmp/fifoCStcl");
		
//		int ccsesf[MAX_ESFERAS];	
//		for(int i=0; i<MAX_ESFERAS; i++)
//		{
//			char cadesf[100];
//			sprintf(cadesf,"/tmp/fifoCsEsf%d",i);
		
//			unlink(cadesf);
//		}
		
//		unlink("/tmp/fifoCSdisp1");
		
//		unlink("/tmp/fifoCSdisp2");
		
//		unlink("/tmp/fifoCScrono");
		
//		unlink("/tmp/fifoCSIA");
		
		exit(0);
		
	}
	//Jugador2
	if(puntos2 >= 11)		//Susceptible a cambio para testing
	{
		sprintf(cadfifo, "Enhorabuena jugador2, has ganado la partida");
		
		if(write(loggerfifo, cadfifo, (strlen(cadfifo)+1)*sizeof(char)) == -1)
		{
			perror("Error en la escritura");
			exit(1);
		}
		
//		unlink("/tmp/fifoCS");
		
//		unlink("/tmp/fifoCStcl");
		
//		int ccsesf[MAX_ESFERAS];	
//		for(int i=0; i<MAX_ESFERAS; i++)
//		{
//			char cadesf[100];
//			sprintf(cadesf,"/tmp/fifoCsEsf%d",i);
		
//			unlink(cadesf);
//		}
		
//		unlink("/tmp/fifoCSdisp1");
		
//		unlink("/tmp/fifoCSdisp2");
		
//		unlink("/tmp/fifoCScrono");
		
//		unlink("/tmp/fifoCSIA");		
		
		exit(0);
	}
	
	//Para comunicacion entre cliente y servidor
	
	char cadcs[200];
	sprintf(cadcs,"%f %f %f %f %f %f %f %f %f %f %d %d %d %d %d %d", esfera.centro.x, esfera.centro.y, 
			jugador1.x1, jugador1.y1, jugador1.x2, jugador1.y2, jugador2.x1, jugador2.y1,
			jugador2.x2, jugador2.y2, puntos1, puntos2, nEsferas, disparo1A, disparo2A, botActivo);
			
	socketjugador1.Send(cadcs,sizeof(cadcs));
	
	char cadesferas[400];
	if(nEsferas >= 1)
	{
		sprintf(cadesferas,"%f %f %f %c %c %c", esferas[0]->centro.x, esferas[0]->centro.y,
					esferas[0]->radio, esferas[0]->red, esferas[0]->green, esferas[0]->blue);
					
		socketjugador1.Send(cadesferas,sizeof(cadesferas));
	}
	
	if(nEsferas >= 2)
	{
		sprintf(cadesferas,"%f %f %f %c %c %c", esferas[1]->centro.x, esferas[1]->centro.y,
					esferas[1]->radio, esferas[1]->red, esferas[1]->green, esferas[1]->blue);
					
		socketjugador1.Send(cadesferas,sizeof(cadesferas));
	}
	
	if(nEsferas >= 3)
	{
		sprintf(cadesferas,"%f %f %f %c %c %c", esferas[2]->centro.x, esferas[2]->centro.y,
					esferas[2]->radio, esferas[2]->red, esferas[2]->green, esferas[2]->blue);
					
		socketjugador1.Send(cadesferas,sizeof(cadesferas));
	}
	
	if(nEsferas >= 4)
	{
		sprintf(cadesferas,"%f %f %f %c %c %c", esferas[3]->centro.x, esferas[3]->centro.y,
					esferas[3]->radio, esferas[3]->red, esferas[3]->green, esferas[3]->blue);
					
		socketjugador1.Send(cadesferas,sizeof(cadesferas));
	}
	
	if(disparo1A == 1)
	{
		sprintf(cadesferas,"%f %f %f %c %c %c", disparo1->centro.x, disparo1->centro.y,
					disparo1->radio, disparo1->red, disparo1->green, disparo1->blue);
					
		socketjugador1.Send(cadesferas,sizeof(cadesferas));
	}
	
	if(disparo2A == 1)
	{
		sprintf(cadesferas,"%f %f %f %c %c %c", disparo2->centro.x, disparo2->centro.y,
					disparo2->radio, disparo2->red, disparo2->green, disparo2->blue);
					
		socketjugador1.Send(cadesferas,sizeof(cadesferas));
	}
	
//	int wcs = write(fdcoordenadas,cadcs,sizeof(cadcs));
//	if(wcs==-1)
//	{
//		perror("Error:Escritura en FIFO CS en servidor");
//	}

//	char cadesf[MAX_ESFERAS][150];
//	int wcsesf[MAX_ESFERAS];
	
//	for(int i=0; i<MAX_ESFERAS; i++)
//	{
//		if(nEsferas >= i+1)
//		{
//			sprintf(cadesf[i],"%f %f %f %c %c %c", esferas[i]->centro.x, esferas[i]->centro.y,
//					esferas[i]->radio, esferas[i]->red, esferas[i]->green, esferas[i]->blue);
					
//			wcsesf[i] = write(fdesferas[i],cadesf[i],sizeof(cadesf[i]));
//			if(wcsesf[i]==-1)
//			{
//				perror("Error:Escritura en FIFO en servidor");
//			}			
//		}
//	}
	
//	if(disparo1A == 1)
//	{
//		char caddisp1[200];
//		sprintf(caddisp1,"%f %f %f %c %c %c",disparo1->centro.x, disparo1->centro.y, disparo1->radio,
//				disparo1->red, disparo1->green, disparo1->blue);
				
//		int wcsdisp1 = write(fddisparo1,caddisp1,sizeof(caddisp1));
//		if(wcsdisp1==-1)
//		{
//			perror("Error:Escritura en FIFO en servidor");
//		}
//	}
	
//	if(disparo2A == 1)
//	{
//		char caddisp2[200];
//		sprintf(caddisp2,"%f %f %f %c %c %c",disparo2->centro.x, disparo2->centro.y, disparo2->radio,
//				disparo2->red, disparo2->green, disparo2->blue);
//				
//		int wcsdisp2 = write(fddisparo2,caddisp2,sizeof(caddisp2));
//		if(wcsdisp2==-1)
//		{
//			perror("Error:Escritura en FIFO en servidor");
//		}
//	}
	
//	if(botActivo == 1)
//	{
//		char cadIA[10];
//		read(fdIA,cadIA,sizeof(cadIA));
		
//		sscanf(cadIA,"%d",&IA);
		
/*		switch(IA)
		{
			case 1: jugador2.DisminuyeVelocidad(1);break;
			case 0: break;
			case -1: jugador2.AumentaVelocidad(1);break;
			case 2: CreaDisparo(jugador2.centro, -4.0f, 2);break;
			default: break;
		}*/
		
//		if(IA == 1) jugador2.DisminuyeVelocidad(1);
//		if(IA == 0);
//		if(IA == -1) jugador2.AumentaVelocidad(1);
//		if(IA == 2) CreaDisparo(jugador2.centro, -4.0f, 2);
//	}

	if(nJugadoresConect == 3)
	{
		socketjugador2=socketconexion.Accept();
		char nombre2[100];
		if(socketjugador2.Receive(nombre2,sizeof(nombre2)) < 0)
		{
			printf("Esperando conexion jugador2\n");
		}
	}

}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
//	switch(key)
//	{
//	case 'q':jugador1.DisminuyeVelocidad(0);break; //Jugador1
//	case 'e':jugador1.AumentaVelocidad(0);break; //Jugador1
//	case 's':jugador1.DisminuyeVelocidad(1);break; //Jugador1
//	case 'w':jugador1.AumentaVelocidad(1);break; //Jugador1
//	case 'l':jugador2.DisminuyeVelocidad(1);/*DesactivaBot();*/break; //Jugador2
//	case 'o':jugador2.AumentaVelocidad(1);/*DesactivaBot();*/break; //Jugador2
//	case 'i':jugador2.DisminuyeVelocidad(0);/*DesactivaBot();*/break; //Jugador2
//	case 'p':jugador2.AumentaVelocidad(0);/*DesactivaBot();*/break; //Jugador2
//	case 'd':CreaDisparo(jugador1.centro, 4.0f, 1);break; //Jugador1
//	case 'k':CreaDisparo(jugador2.centro, -4.0f, 2);/*DesactivaBot();*/break; //Jugador2
//	case 'h':pausa = not(pausa); break; //None of them

//	default:break;

//	}
}

void CMundo::Init()
{
	nRebotes = 0;
	nEsferas = 0;

	disparo1A = 0;
	disparo2A = 0;
	
	pausa = false;
	
	contador = 0.0f;
	botActivo = 0;
	
	doblevelocidad = 0;
	
	nJugadoresConect = 0;
	
	const char namepipe[] = "/tmp/pipe";
//	const char namebot[] = "/tmp/ficheroproyeccion.txt";
	
	loggerfifo = open(namepipe, O_WRONLY);
	if(loggerfifo == -1)
	{
		perror("Error al abrir el pipe");
		exit(1);
	}
	
//	pMemCompartida = &memCompartida;
//	botfichero = open(namebot, O_RDWR|O_CREAT|O_TRUNC, 0666);
//	if(botfichero == -1)
//	{
//		perror("Error al abrir el fichero proyectado");
//		exit(1);
//	}
//	if(write(botfichero, pMemCompartida, sizeof(memCompartida)) == -1)
//	{
//		perror("Error al escribir en el fichero proyectado");
//	}
	
//	botproyeccion = (char *)mmap(NULL, sizeof(memCompartida), PROT_WRITE|PROT_READ, MAP_SHARED, botfichero, 0);
//	close(botfichero);
//	pMemCompartida = (DatosMemCompartida *)botproyeccion;
//	pMemCompartida->accion = 0;
	
//	lamasCercana = &esfera;

	//Para la comunicacion cliente servidor para las coordenadas

//	fdcoordenadas = open("/tmp/fifoCS", O_WRONLY);
//	if(fdcoordenadas==-1)
//	{
//		perror("Error: Apertura FIFO CS en servidor para coordenadas");
//	}

	//para los thread
	
	pthread_create(&hilo, NULL, hilo_comandos, this);

	//Para la comunicacion cliente servidor para las teclas
	
//	fdtecla = open("/tmp/fifoCStcl", O_RDONLY);
//	if(fdtecla==-1)
//	{
//		perror("Error: Apertura FIFO CS en servidor para teclas");
//	}
	
	//Para la comunicacion cliente servidor para las esferas
	
//	char cadesf[MAX_ESFERAS][100];
//	for(int i=0; i<MAX_ESFERAS; i++)
//	{
//		sprintf(cadesf[i],"/tmp/fifoCsEsf%d",i);
//		
//		fdesferas[i] = open(cadesf[i],O_WRONLY);
//		if(fdesferas[i]==-1)
//		{
//			char cad[100];
//			sprintf(cad,"Error: Apertura FIFO CS en esfera%d en servidor",i+1);
//			perror(cad);
//		}		
//	}
	
	//
	
//	fddisparo1 = open("/tmp/fifoCSdisp1",O_WRONLY);
//	if(fddisparo1==-1)
//	{
//		perror("Error: Apertura FIFO CS en disparo1 en cliente");
//	}
	
	//
	
//	fddisparo2 = open("/tmp/fifoCSdisp2",O_WRONLY);
//	if(fddisparo2==-1)
//	{
//		perror("Error: Apertura FIFO CS en disparo2 en cliente");
//	}
	
	//
	
//	fdcrono = open("/tmp/fifoCScrono",O_RDONLY);
//	if(fdcrono==-1)
//	{
//		perror("Error: Apertura FIFO CS en crono en cliente");
//	}
	
	//
	
//	fdIA = open("/tmp/fifoCSIA",O_RDONLY);
//	if(fdIA==-1)
//	{
//		perror("Error: Apertura FIFO CS en IA en cliente");
//	}

//Para la comunicacion por sockets de las teclas

	socketconexion.InitServer((char *)"127.0.0.1",4800);
	socketjugador1=socketconexion.Accept();
	char nombre[100];
	socketjugador1.Receive(nombre,sizeof(nombre));
	printf("Cliente: %s\n",nombre);
	strcpy(nombrejugador1,nombre);
	nJugadoresConect++;
	
//	socketconexion2.InitServer((char *)"127.0.0.1",4800);
//	socketjugador2=socketconexion2.Accept();
	
//Para la comunicacion por sockets del crono y de la IA

//	socketcr.InitServer((char *)"127.0.0.2",4800);
//	socketcrono=socketcr.Accept();
	
	
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
}

void CMundo::CreaEsferas(void)
{
	if(nEsferas <= MAX_ESFERAS)
	{
		esferas[nEsferas] = new Esfera(20); //El numero da igual, solo es para diferenciar los constructores
		nEsferas++;
	}
}

void CMundo::DestruyeEsferas(void)
{
	if(nEsferas > 0)
	{
		sprintf(cadfifo, "Esferas destruidas");
	
		if(write(loggerfifo, cadfifo, (strlen(cadfifo)+1)*sizeof(char)) == -1)
		{
			perror("Error en la escritura");
			exit(1);
		}
	}
	
	lamasCercana = &esfera;
	
	int i;
	for(i=0; i<nEsferas; i++)
	{
		delete esferas[i];
	}
	nEsferas = 0;
	
	
}

void CMundo::CreaDisparo(Vector2D centro, float velocidadX, int jugador)
{
	switch(jugador)
	{
		case 1: if(disparo1A == 0)
				{
					disparo1 = new Esfera(centro, velocidadX);
					disparo1A = 1;
					
					sprintf(cadfifo, "Jugador 1 dispara");
		
					if(write(loggerfifo, cadfifo, (strlen(cadfifo)+1)*sizeof(char)) == -1)
					{
						perror("Error en la escritura");
						exit(1);
					}			
				}
				break;
		case 2: if(disparo2A == 0)
				{
					disparo2 = new Esfera(centro, velocidadX);
					disparo2A = 1;
					
					sprintf(cadfifo, "Jugador 2 dispara");
		
					if(write(loggerfifo, cadfifo, (strlen(cadfifo)+1)*sizeof(char)) == -1)
					{
						perror("Error en la escritura");
						exit(1);
					}	
				}
				break;
		default:break;	
	}
}

void CMundo::DesactivaBot(void)
{
	botActivo = 0;
//	contador = 0.0f;
}

