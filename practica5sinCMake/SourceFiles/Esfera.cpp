// Esfera.cpp: implementation of the Esfera class.
//
//////////////////////////////////////////////////////////////////////

#include "../HeaderFiles/Esfera.h"
#include "../HeaderFiles/glut.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Esfera::Esfera()
{
	radio=0.5f;
	velocidad.x=3;
	velocidad.y=3;
	red = 255;
	green = 255;
	blue = 0;
}

Esfera::Esfera(unsigned int n)
{
	radio=0.5f;
	velocidad.x=3;
	velocidad.y=-3;
	red = 0;
	green = 255;
	blue = 0;
}

Esfera::Esfera(Vector2D cent, float velx)
{
	centro = cent;
	radio=0.25f;
	velocidad.x=velx;
	velocidad.y=0.0f;
	red = 255;
	green = 0;
	blue = 0;
}

Esfera::~Esfera()
{

}



void Esfera::Dibuja()
{
	glColor3ub(red,green,blue);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(centro.x,centro.y,0);
	glutSolidSphere(radio,15,15);
	glPopMatrix();
}

void Esfera::Dibuja2()
{
	glColor3ub(0,255,0);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(centro.x,centro.y,0);
	glutSolidSphere(radio,15,15);
	glPopMatrix();
}

void Esfera::Dibuja3()
{
	glColor3ub(255,0,0);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(centro.x,centro.y,0);
	glutSolidSphere(radio,15,15);
	glPopMatrix();
}

void Esfera::Mueve(float t)
{
//	centro.x += velocidad.x*t;
//	centro.y += velocidad.y*t;

	centro += velocidad*t;

}

bool Esfera::Rebota(Esfera *e)
{
	float dist = (centro - (e->centro)).modulo();
	if(dist <= (radio + (e->radio)))
	{
		Vector2D velIni1 = velocidad;
		Vector2D velIni2 = (e->velocidad);
		
		Vector2D velFin1 = velIni1*(radio - (e->radio));
		velFin1 += velIni2*(e->radio)*2;
		velFin1 = velFin1/(radio + (e->radio));
		velocidad = velFin1;
		
		Vector2D velFin2 = velIni2*((e->radio) - radio);
		velFin2 += velIni1*radio*2;
		velFin2 = velFin2/(radio + (e->radio));
		e->velocidad = velFin2;
		
		Vector2D vec12 = centro - (e->centro);
		dist -= radio;
		dist -= (e->radio);
		if(vec12.modulo() > 0)
		{
			centro = (centro - vec12.Unitario()*dist/2);
			e->centro = ((e->centro) - vec12.Unitario()*dist/2);
		}
		return true;
		
	}
	return false;
}
