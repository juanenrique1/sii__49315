// Mundo.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"
#include "DatosMemCompartida.h"
#include "Socket.h"

#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <unistd.h>

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define MAX_ESFERAS 4

class CMundo  
{
public:
	void Init();
	CMundo();
	virtual ~CMundo();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	

	Esfera esfera;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;

	int puntos1;
	int puntos2;

	int nRebotes;
	int nEsferas;

	Esfera esferas[MAX_ESFERAS];		//Quitamos los punteros del cliente

	void CreaEsferas(void);
	void DestruyeEsferas(void);

	Esfera disparo1;
	int disparo1A;   //true si disparo1 esta activo, false si esta inactivo
	Esfera disparo2;
	int disparo2A;   //true si disparo2 esta activo, false si esta inactivo

	void CreaDisparo(Vector2D centro, float velocidadX, int jugador);

	bool pausa;	//true si modo pausa esta activo, false si esta inactivo

	char cadfifo[200];

	int loggerfifo;

	//PARTE BOT

	int botfichero;

	DatosMemCompartida memCompartida;
	DatosMemCompartida *pMemCompartida;

	Esfera *lamasCercana;

	char *botproyeccion;

	float contador; //Para hacer la cuenta del bot de los 10 segundos
	int botActivo;

	void DesactivaBot(void); //Para desactivar el bot una vez se haya pulsado una tecla

	//COMUNICACION CLIENTE SERVIDOR

	int fdcoordenadas;
	int fdtecla;
	int fdesferas[MAX_ESFERAS];
	int fddisparo1;
	int fddisparo2;
	int fdcrono;
	int fdIA;

	Socket socketcliente;

	Socket socketcrono;


};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
