// Raqueta.h: interface for the Raqueta class.
//
//////////////////////////////////////////////////////////////////////

#include "Plano.h"
#include "Vector2D.h"

class Raqueta : public Plano  
{
public:
	Vector2D velocidad;
	Vector2D centro;

	void SetCentro(void);

	Raqueta();
	virtual ~Raqueta();

	void Mueve(float t);
	void AumentaVelocidad(int n); // x -> n=0 || y -> n=1;
	void DisminuyeVelocidad(int n); // x -> n=0 || y -> n=1;
};
