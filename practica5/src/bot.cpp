#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

#include "DatosMemCompartida.h"

int main(void)
{
	int file;
	
//	DatosMemCompartida memCompartida;
	DatosMemCompartida *pMemCompartida /*= &memCompartida*/;
	
	char *proyeccion;
	
	const char *ficheroproyeccion = "/tmp/ficheroproyeccion.txt";
	file = open(ficheroproyeccion, O_RDWR);
	
	//void *mmap(void *addr, size_t length, int prot, int flags, int fd, off_t offset);
	proyeccion = (char *)mmap(NULL, sizeof((pMemCompartida)), PROT_READ|PROT_WRITE, MAP_SHARED, file, 0x00);
	
	close(file);
	
	pMemCompartida = (DatosMemCompartida*) proyeccion;
	
	while(1)
	{
		float mitadraqueta = ((pMemCompartida->raqueta1.y2+pMemCompartida->raqueta1.y1)/2.0f);
		
		if(pMemCompartida->esfera.centro.y < mitadraqueta)
			pMemCompartida->accion = 1;
		
		if(pMemCompartida->esfera.centro.y == mitadraqueta)
			pMemCompartida->accion = 0;
			
		if(pMemCompartida->esfera.centro.y > mitadraqueta)
			pMemCompartida->accion = -1;
			
		if(pMemCompartida->disparo2A == false)
			pMemCompartida->accion = 2;
			
		usleep(25000);
	}
	
	munmap(proyeccion, sizeof(*(pMemCompartida)));
	
	exit(0);
	
	return 0;
}
