// Raqueta.cpp: implementation of the Raqueta class.
//
//////////////////////////////////////////////////////////////////////

#include "Raqueta.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Raqueta::Raqueta()
{
	
}

Raqueta::~Raqueta()
{

}

void Raqueta::Mueve(float t)
{
	x1 += velocidad.x*t;
	x2 += velocidad.x*t;
	y1 += velocidad.y*t;
	y2 += velocidad.y*t;

}

void Raqueta::AumentaVelocidad(int n)
{
	switch(n)
	{
		case 0:velocidad.x=1;break;
		case 1:velocidad.y=4;break;
		default: break;
	}
}

void Raqueta::DisminuyeVelocidad(int n)
{
	switch(n)
	{
		case 0:velocidad.x=-1;break;
		case 1:velocidad.y=-4;break;
		default: break;
	}
}

void Raqueta::SetCentro(void)
{
	centro.x = x1;
	centro.y = (y1+y2)/2.0f;
}
