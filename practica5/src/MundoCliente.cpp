// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
 	//Cierre de la proyección de memoria
	munmap(botproyeccion,sizeof(memCompartida));
	
	//cerrar el fifo de comunicacion entre cliente y servidor para coordenadas
	
//	int ccsc = close(fdcoordenadas);
//	if(ccsc==-1)
//	{
//		perror("Error: Cerrar tuberia CS en Mundo cliente para coordenadas");
//	}
	
//	unlink("/tmp/fifoCS");
	
	//cerrar el fifo de comunicacion entre cliente y servidor para teclas
	
//	int ccst = close(fdtecla);
//	if(ccst==-1)
//	{
//		perror("Error: Cerrar tuberia CS en Mundo cliente para teclas");
//	}
	
//	unlink("/tmp/fifoCStcl");
	
//	int ccsesf[MAX_ESFERAS];
	
//	for(int i=0; i<MAX_ESFERAS; i++)
//	{
//		ccsesf[i] = close(fdesferas[i]);
//		if(ccsesf[i]==-1)
//		{
//			char cad[100];
//			sprintf(cad,"Error: Cerrar tuberia CS en Mundo Cliente para esfera%d",i);
//			perror(cad);
//		}
		
//		char cadesf[100];
//		sprintf(cadesf,"/tmp/fifoCsEsf%d",i);
		
//		unlink(cadesf);
//	}
	
//	int ccsdisp1 = close(fddisparo1);
//	if(ccsdisp1==-1)
//	{
//		perror("Error: Cerrar tuberia CS en Mundo cliente para disparo1");
//	}
	
//	unlink("/tmp/fifoCSdisp1");
	
//	int ccsdisp2 = close(fddisparo2);
//	if(ccsdisp2==-1)
//	{
//		perror("Error: Cerrar tuberia CS en Mundo cliente para disparo2");
//	}
	
//	unlink("/tmp/fifoCSdisp2");
	
//	int ccscrono = close(fdcrono);
//	if(ccscrono==-1)
//	{
//		perror("Error: Cerrar tuberia CS en Mundo cliente para crono");
//	}
	
//	unlink("/tmp/fifoCScrono");
	
//	int ccsIA = close(fdIA);
//	if(fdIA==-1)
//	{
//		perror("Error: Cerrar tuberia CS en Mundo cliente para IA");
//	}
	
//	unlink("/tmp/fifoCSIA");
	
//	ccsesf[0] = close(fdesferas[0]);
//	if(ccsesf[0]==-1)
//	{
//		perror("Error: Cerrar tuberia CS en Mundo cliente para esfera0");
//	}
	
//	unlink("/tmp/fifoCsEsf1");
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	
	/////////// MODO PAUSA ////////
	char cadpausa[] = "PAUSE (Pulse H para continuar)";
	if(pausa == true)
	{
		print(cadpausa,300,300,1,1,1);
    }
    
    /////////// MODO BOT //////////
    char cadBot[] = "BOT activado";
    if(botActivo == 1)
    {
		print(cadBot,400,0,0,255,0);
	}

	int i,j;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();
	
	for(i=0; i<nEsferas; i++)
	{
		esferas[i].Dibuja2();
	}
	
	if(disparo1A == true) disparo1.Dibuja3();
	if(disparo2A == true) disparo2.Dibuja3();
	

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{
/*	char cadcrono[50];
	if(botActivo == 0)
	{
		sprintf(cadcrono,"%f",contador);
//		int wcscrono = write(fdcrono,cadcrono,sizeof(cadcrono));
//		if(wcscrono==-1)
//		{
//			perror("Error: Escritura FIFO CS en cliente");
//		}

		socketcliente.Send(cadcrono,sizeof(cadcrono));
	}
	
	if(botActivo == 1)
	{
		sprintf(cadcrono,"%f %d",contador,pMemCompartida->accion);
//		int wcscrono = write(fdcrono,cadcrono,sizeof(cadcrono));
//		if(wcscrono==-1)
//		{
//			perror("Error: Escritura FIFO CS en cliente");
//		}

		socketcliente.Send(cadcrono,sizeof(cadcrono));		
	}
*/		
//	jugador1.Mueve(0.025f);
//	jugador2.Mueve(0.025f);
	
//	jugador1.SetCentro();
//	jugador2.SetCentro();
	
//	esfera.Mueve(0.025f);
	
//	int i;
//	int j;
	
//	for(i=0; i<nEsferas; i++)
//	{
//		esferas[i]->Mueve(0.025f);
//	}
	
//	if(disparo1A == true) disparo1->Mueve(0.025f);
//	if(disparo2A == true) disparo2->Mueve(0.025f);
	
/*	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}*/
	
/*	for(i=0;i<paredes.size();i++)
	{
		for(j=0; j<nEsferas; j++)
		{
			paredes[i].Rebota(*(esferas[j]));
		}
	}*/
	
/*	if(disparo1A == true)
	{
		for(i=0;i<paredes.size();i++)
		{
			paredes[i].Rebota(*(disparo1));
		}
	}*/
	
/*	if(disparo2A == true)
	{
		for(i=0;i<paredes.size();i++)
		{
			paredes[i].Rebota(*(disparo2));
		}
	}*/

//	jugador1.Rebota(esfera);
//	jugador2.Rebota(esfera);

//	if(jugador1.Rebota(esfera))nRebotes++;
//	if(jugador2.Rebota(esfera))nRebotes++;
	
/*	for(i=0; i<nEsferas; i++)
	{
		if(jugador1.Rebota(*(esferas[i])))nRebotes++;
		if(jugador2.Rebota(*(esferas[i])))nRebotes++;
	}*/


/*	if(fondo_izq.Rebota(esfera))
	{
		DestruyeEsferas();
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		
//		sprintf(cadfifo, "Jugador 2 anota un punto, lleva %d puntos", puntos2);
			
//		if(write(loggerfifo, cadfifo, (strlen(cadfifo)+1)*sizeof(char)) == -1)
//		{
//			perror("Error en la escritura");
//			exit(1);
//		}
		
	}*/

/*	if(fondo_dcho.Rebota(esfera))
	{
		DestruyeEsferas();
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
		
//		sprintf(cadfifo, "Jugador 1 anota un punto, lleva %d puntos", puntos1);
			
//		if(write(loggerfifo, cadfifo, (strlen(cadfifo)+1)*sizeof(char)) == -1)
//		{
//			perror("Error en la escritura");
//			exit(1);
//		}
				
	}*/
	
/*	for(i=0; i<nEsferas; i++)
	{
		if(fondo_izq.Rebota(*(esferas[i])))
		{
			DestruyeEsferas();
			esfera.centro.x=0;
			esfera.centro.y=rand()/(float)RAND_MAX;
			esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
			esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
			puntos2++;
			
//			sprintf(cadfifo, "Jugador 2 anota un punto, lleva %d puntos", puntos2);
			
//			if(write(loggerfifo, cadfifo, (strlen(cadfifo)+1)*sizeof(char)) == -1)
//			{
//				perror("Error en la escritura");
//				exit(1);
//			}

		}

		if(fondo_dcho.Rebota(*(esferas[i])))
		{
			DestruyeEsferas();
			esfera.centro.x=0;
			esfera.centro.y=rand()/(float)RAND_MAX;
			esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
			esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
			puntos1++;
			
//			sprintf(cadfifo, "Jugador 1 anota un punto, lleva %d puntos", puntos1);
			
//			if(write(loggerfifo, cadfifo, (strlen(cadfifo)+1)*sizeof(char)) == -1)
//			{
//				perror("Error en la escritura");
//				exit(1);
//			}

		}	
	}*/
	
/*	if(disparo1A == true)
	{
		if(fondo_dcho.Rebota(*(disparo1)))
		{
			delete disparo1;
			disparo1A = false;
			puntos1++;
			
//			sprintf(cadfifo, "Jugador 2 no ha podido detener disparo, Jugador 1 consigue un punto, lleva %d puntos", puntos1);
			
//			if(write(loggerfifo, cadfifo, (strlen(cadfifo)+1)*sizeof(char)) == -1)
//			{
//				perror("Error en la escritura");
//				exit(1);
//			}
		}
		
		if(jugador2.Rebota(*(disparo1)))
		{
			delete disparo1;
			disparo1A = false;
			puntos2++;
			
//			sprintf(cadfifo, "Jugador 2 ha detenido el disparo, Jugador 2 consigue un punto, lleva %d puntos", puntos2);
			
//			if(write(loggerfifo, cadfifo, (strlen(cadfifo)+1)*sizeof(char)) == -1)
//			{
//				perror("Error en la escritura");
//				exit(1);
//			}
		}
	}*/
	
/*	if(disparo2A == true)
	{
		if(fondo_izq.Rebota(*(disparo2)))
		{
			delete disparo2;
			disparo2A = false;
			puntos2++;
			
//			sprintf(cadfifo, "Jugador 1 no ha podido detener disparo, Jugador 2 consigue un punto, lleva %d puntos", puntos2);
			
//			if(write(loggerfifo, cadfifo, (strlen(cadfifo)+1)*sizeof(char)) == -1)
//			{
//				perror("Error en la escritura");
//				exit(1);
//			}
		}
		
		if(jugador1.Rebota(*(disparo2)))
		{
			delete disparo2;
			disparo2A = false;
			puntos1++;
			
//			sprintf(cadfifo, "Jugador 1 ha detenido el disparo, Jugador 1 consigue un punto, lleva %d puntos", puntos1);
			
//			if(write(loggerfifo, cadfifo, (strlen(cadfifo)+1)*sizeof(char)) == -1)
//			{
//				perror("Error en la escritura");
//				exit(1);
//			}
		}
	}*/
	
	
/*	if(nRebotes >= 3) 
	{
		CreaEsferas();
		nRebotes = 0;
		
//		sprintf(cadfifo, "Esfera creada, hay %d esferas", nEsferas+1);
		
//		if(write(loggerfifo, cadfifo, (strlen(cadfifo)+1)*sizeof(char)) == -1)
//		{
//			perror("Error en la escritura");
//			exit(1);
//		}
	}*/
	
/*	for(i=0; i<nEsferas; i++)
	{
		esfera.Rebota(esferas[i]);
	}*/
	
/*	for(i=0; i<nEsferas; i++)
	{
		for(j=i+1; j<nEsferas; j++)
		{
			esferas[i]->Rebota(esferas[j]);
		}
	}*/
	
/*	if(disparo1A == true && (esfera.Rebota(disparo1)))
	{
		delete disparo1;
		disparo1A = false;
	}
	if(disparo2A == true && (esfera.Rebota(disparo2)))
	{
		delete disparo2;
		disparo2A = false;
	}*/

/*	for(i=0; i<nEsferas; i++)
	{
		if(disparo1A == true && (esferas[i]->Rebota(disparo1)))
		{
			delete disparo1;
			disparo1A = false;
		}
		if(disparo2A == true && (esferas[i]->Rebota(disparo2)))
		{
			delete disparo2;
			disparo2A = false;
		}
	}*/
	
	//ENCAPSULAMIENTO RAQUETAS//
	
	//Jugador1
	
//	if((jugador1.x1 <= -7) || (jugador1.x1 >= 7))
//	{
//		jugador1.g=0;
//		jugador1.x1=-6;jugador1.y1=-1;
//		jugador1.x2=-6;jugador1.y2=1;
//		jugador1.velocidad.x = 0;
//		jugador1.velocidad.y = 0;
//	}
	
	//Jugador2
	
//	if((jugador2.x1 <= -7) || (jugador2.x1 >= 7))
//	{
//		jugador2.g=0;
//		jugador2.x1=6;jugador2.y1=-1;
//		jugador2.x2=6;jugador2.y2=1;
//		jugador2.velocidad.x = 0;
//		jugador2.velocidad.y = 0;
//	}
	
	//LOGGER PAUSE Todavia en pruebas
	
/*	if(pausa == true)
	{
		sprintf(cadfifo, "Modo pausa activado");
		
		if(write(loggerfifo, cadfifo, (strlen(cadfifo)+1)*sizeof(char)) == -1)
		{
			perror("Error en la escritura");
			exit(1);
		}		
	}*/
	
	if(botActivo == 1)
	{
		switch(pMemCompartida->accion)
		{
			case 1:
//				OnKeyboardDown('l', 0, 0);
				botActivo = 1;
				break;
			case 0:
				botActivo = 1;
				break;
			case -1:
//				OnKeyboardDown('o', 0, 0);
				botActivo = 1;
				break;
			case 2:
//				OnKeyboardDown('k', 0, 0);
				botActivo = 1;
				break;
			default:
				break;
		}
		
//		char cadIA[10];
//		sprintf(cadIA,"%d",pMemCompartida->accion);
		
//		int wcsIA = write(fdIA, cadIA, sizeof(cadIA));
//		if(wcsIA==-1)
//		{
//			perror("Error: Escritura FIFO CS en cliente en IA");
//		}
	}
	
	//Comprobamos que esfera es la mas cercana
	
	for(int i=0; i<nEsferas; i++)
	{
		if(esferas[i].centro.x > lamasCercana->centro.x)
			lamasCercana = &esferas[i];
	}
	
	if(esfera.centro.x > lamasCercana->centro.x)
		lamasCercana = &esfera;
		
	if((disparo1A == 1) && (disparo1.centro.x > lamasCercana->centro.x))
		lamasCercana = &disparo1;
		
	//Esto es para solucionar el problema del estacamiento del bot
	//Con memoria dinamica no es necesario pero al transformarlo en
	//memoria estatica hay que ponerlo o el bot se atasca	
	if(nEsferas == 0)
		lamasCercana = &esfera;
	
	//
	
	pMemCompartida->esfera = *lamasCercana;
	pMemCompartida->raqueta1 = jugador2;
	pMemCompartida->disparo2A = this->disparo2A;
	
	contador+=0.025f;
	
//	if(contador >= 15.0f)
//		botActivo = true;
		
		
	//Salida del juego si un jugador gana la partida por mas de 7 puntos
	//Jugador1
//	if(puntos1 >= 11)
//	{
//		sprintf(cadfifo, "Enhorabuena jugador1, has ganado la partida");
		
//		if(write(loggerfifo, cadfifo, (strlen(cadfifo)+1)*sizeof(char)) == -1)
//		{
//			perror("Error en la escritura");
//			exit(1);
//		}
		
//		exit(0);
//	}
	//Jugador2
//	if(puntos2 >= 11)
//	{
//		sprintf(cadfifo, "Enhorabuena jugador2, has ganado la partida");
		
//		if(write(loggerfifo, cadfifo, (strlen(cadfifo)+1)*sizeof(char)) == -1)
//		{
//			perror("Error en la escritura");
//			exit(1);
//		}
		
//		exit(0);
//	}
	
	char cadcs[200];
//	read(fdcoordenadas,cadcs,sizeof(cadcs));
	socketcliente.Receive(cadcs,sizeof(cadcs));
	
	sscanf(cadcs,"%f %f %f %f %f %f %f %f %f %f %d %d %d %d %d %d", &esfera.centro.x,&esfera.centro.y,
			&jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2, &jugador2.x1,&jugador2.y1,
			&jugador2.x2,&jugador2.y2, &puntos1, &puntos2, &nEsferas, &disparo1A, &disparo2A, &botActivo);
			
	char cadesferas[400];
	if(nEsferas >= 1)
	{
		socketcliente.Receive(cadesferas,sizeof(cadesferas));
		
		sscanf(cadesferas,"%f %f %f %c %c %c", &esferas[0].centro.x,&esferas[0].centro.y,
					&esferas[0].radio, &esferas[0].red, &esferas[0].green, &esferas[0].blue);
	}
	
	if(nEsferas >= 2)
	{
		socketcliente.Receive(cadesferas,sizeof(cadesferas));
		
		sscanf(cadesferas,"%f %f %f %c %c %c", &esferas[1].centro.x,&esferas[1].centro.y,
					&esferas[1].radio, &esferas[1].red, &esferas[1].green, &esferas[1].blue);
	}
	
	if(nEsferas >= 3)
	{
		socketcliente.Receive(cadesferas,sizeof(cadesferas));
		
		sscanf(cadesferas,"%f %f %f %c %c %c", &esferas[2].centro.x,&esferas[2].centro.y,
					&esferas[2].radio, &esferas[2].red, &esferas[2].green, &esferas[2].blue);
	}
	
	if(nEsferas >= 4)
	{
		socketcliente.Receive(cadesferas,sizeof(cadesferas));
		
		sscanf(cadesferas,"%f %f %f %c %c %c", &esferas[3].centro.x,&esferas[3].centro.y,
					&esferas[3].radio, &esferas[3].red, &esferas[3].green, &esferas[3].blue);
	}
		
	if(disparo1A == 1)
	{
		socketcliente.Receive(cadesferas,sizeof(cadesferas));
		
		sscanf(cadesferas,"%f %f %f %c %c %c", &disparo1.centro.x,&disparo1.centro.y,
					&disparo1.radio, &disparo1.red, &disparo1.green, &disparo1.blue);		
	}
	
	if(disparo2A == 1)
	{
		socketcliente.Receive(cadesferas,sizeof(cadesferas));
		
		sscanf(cadesferas,"%f %f %f %c %c %c", &disparo2.centro.x,&disparo2.centro.y,
					&disparo2.radio, &disparo2.red, &disparo2.green, &disparo2.blue);		
	}

//	char cadesf[MAX_ESFERAS][150];
	
//	for(int i=0; i<MAX_ESFERAS; i++)
//	{
//		if(nEsferas >= i+1)
//		{		
//			read(fdesferas[i],cadesf[i],sizeof(cadesf[i]));
//		
//			sscanf(cadesf[i],"%f %f %f %c %c %c", &esferas[i].centro.x, &esferas[i].centro.y,
//					&esferas[i].radio, &esferas[i].red, &esferas[i].green, &esferas[i].blue);
//			
//		}		
//	}
	
//	if(disparo1A == 1)
//	{
//		char caddisp1[200];
//		read(fddisparo1,caddisp1,sizeof(caddisp1));
//		
//		sscanf(caddisp1,"%f %f %f %c %c %c", &disparo1.centro.x, &disparo1.centro.y, &disparo1.radio,
//				&disparo1.red, &disparo1.green, &disparo1.blue); 
//	}
	
//	if(disparo2A == 1)
//	{
//		char caddisp2[200];
//		read(fddisparo2,caddisp2,sizeof(caddisp2));
//		
//		sscanf(caddisp2,"%f %f %f %c %c %c", &disparo2.centro.x, &disparo2.centro.y, &disparo2.radio,
//				&disparo2.red, &disparo2.green, &disparo2.blue); 
//	}
	
	
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
//	contador = 0.0f;

//	printf("Hola Mundo\n");
	
	switch(key)
	{
//	case 'q':jugador1.DisminuyeVelocidad(0);break; //Jugador1
//	case 'e':jugador1.AumentaVelocidad(0);break; //Jugador1
//	case 's':jugador1.DisminuyeVelocidad(1);break; //Jugador1
//	case 'w':jugador1.AumentaVelocidad(1);break; //Jugador1
	case 'l':/*jugador2.DisminuyeVelocidad(1);DesactivaBot();*/contador = 0.0f; break; //Jugador2
	case 'o':/*jugador2.AumentaVelocidad(1);DesactivaBot();*/contador = 0.0f; break; //Jugador2
	case 'i':/*jugador2.DisminuyeVelocidad(0);DesactivaBot();*/contador = 0.0f; break; //Jugador2
	case 'p':/*jugador2.AumentaVelocidad(0);DesactivaBot();*/contador = 0.0f; break; //Jugador2
	case 'd':CreaDisparo(jugador1.centro, 4.0f, 1);break; //Jugador1
	case 'k':CreaDisparo(jugador2.centro, -4.0f, 2);/*DesactivaBot();*/contador = 0.0f; break; //Jugador2
//	case 'h':pausa = not(pausa); break; //None of them

	default:break;

	}
	
	char cadk[1];
	sprintf(cadk,"%c",key);
//	write(fdtecla,cadk,sizeof(cadk)+1);
//	printf("%c\n",key);
	if(socketcliente.Send(cadk,strlen(cadk)+1) == -1)
		printf("Error al enviar la tecla\n");
	else
		printf("La tecla enviada es: %c\n",key);
	printf("%c\n",key);
}

void CMundo::Init()
{
	nRebotes = 0;
	nEsferas = 0;
	
	disparo1A = 0;
	disparo2A = 0;
	
	pausa = false;
	
	contador = 0.0f;
	botActivo = 0;
	
//	const char namepipe[] = "/tmp/pipe";
	const char namebot[] = "/tmp/ficheroproyeccion.txt";
	
//	loggerfifo = open(namepipe, O_WRONLY);
//	if(loggerfifo == -1)
//	{
//		perror("Error al abrir el pipe");
//		exit(1);
//	}
	
	pMemCompartida = &memCompartida;
	botfichero = open(namebot, O_RDWR|O_CREAT|O_TRUNC, 0666);
	if(botfichero == -1)
	{
		perror("Error al abrir el fichero proyectado");
		exit(1);
	}
	if(write(botfichero, pMemCompartida, sizeof(memCompartida)) == -1)
	{
		perror("Error al escribir en el fichero proyectado");
	}
	
	botproyeccion = (char *)mmap(NULL, sizeof(memCompartida), PROT_WRITE|PROT_READ, MAP_SHARED, botfichero, 0);
	close(botfichero);
	pMemCompartida = (DatosMemCompartida *)botproyeccion;
	pMemCompartida->accion = 0;
	
	lamasCercana = &esfera;
	
	//Para la comunicacion cliente servidor transmision de coordenadas
	
//	int mkcsc=mkfifo("/tmp/fifoCS",0777);
//	if(mkcsc==-1)
//	{
//		perror("Error: Creacion FIFO CS pata coordenadas");
//	}
	
//	fdcoordenadas=open("/tmp/fifoCS", O_RDONLY);
//	if(fdcoordenadas==-1)
//	{
//		perror("Error: Apertura FIFO CS de coordenadas en cliente");
//	}

//Para la comunicacion cliente servidor transmision de teclas
	
//	int mkcst = mkfifo("/tmp/fifoCStcl",0777);
//	if(mkcst==-1)
//	{
//		perror("Error: Creacion FIFO CS para teclas");
//	}
	
//	fdtecla = open("/tmp/fifoCStcl", O_WRONLY);
//	if(fdtecla==-1)
//	{
//		perror("Error: Apertura FIFO CS de teclas en cliente");
//	}
	
//Para la comunicacion cliente servidor trasmision de esferas

//	char cadesf[MAX_ESFERAS][100];
//	int mkcsesf[MAX_ESFERAS];
//	for(int i=0; i<MAX_ESFERAS; i++)
//	{
//		sprintf(cadesf[i],"/tmp/fifoCsEsf%d",i);
		
//		mkcsesf[i] = mkfifo(cadesf[i],0777);
//		if(mkcsesf[i]==-1)
//		{
//			char cad[100];
//			sprintf(cad,"Error: Creacion FIFO CS en esfera%d en cliente",i+1);
//			perror(cad);
//		}
		
//		fdesferas[i] = open(cadesf[i],O_RDONLY);
//		if(fdesferas[i]==-1)
//		{
//			char cad[100];
//			sprintf(cad,"Error: Apertura FIFO CS en esfera%d en cliente",i+1);
//			perror(cad);
//		}
//	}
	
	//
	
//	int mkdisp1 = mkfifo("/tmp/fifoCSdisp1",0777);
//	if(mkdisp1==-1)
//	{
//		perror("Error: Creacion FIFO CS en disparo1 en cliente");
//	}
	
//	fddisparo1 = open("/tmp/fifoCSdisp1",O_RDONLY);
//	if(fddisparo1==-1)
//	{
//		perror("Error: Apertura FIFO CS en disparo1 en cliente");
//	}
	
	//

//	int mkdisp2 = mkfifo("/tmp/fifoCSdisp2",0777);
//	if(mkdisp2==-1)
//	{
//		perror("Error: Creacion FIFO CS en disparo2 en cliente");
//	}
	
//	fddisparo2 = open("/tmp/fifoCSdisp2",O_RDONLY);
//	if(fddisparo2==-1)
//	{
//		perror("Error: Apertura FIFO CS en disparo2 en cliente");
//	}
	
	//
	
//	int mkcrono = mkfifo("/tmp/fifoCScrono",0777);
//	if(mkcrono==-1)
//	{
//		perror("Error: Creacion FIFO CS en crono en cliente");
//	}
	
//	fdcrono = open("/tmp/fifoCScrono",O_WRONLY);
//	if(fdcrono==-1)
//	{
//		perror("Error: Apertura FIFO CS en crono en cliente");
//	}
	
	//
	
//	int mkIA = mkfifo("/tmp/fifoCSIA",0777);
//	if(mkIA==-1)
//	{
//		perror("Error: Creacion FIFO CS en IA en cliente");
//	}
	
//	fdIA = open("/tmp/fifoCSIA",O_WRONLY);
//	if(fdIA==-1)
//	{
//		perror("Error: Apertura FIFO CS en IA en cliente");
//	}

	char nombre[200];
	printf("Introduzca su nombre aqui: ");
	scanf("%s",nombre);
//	socketcliente.Connect((char *)"127.0.0.1",4800);
//	socketcliente.Send(nombre,strlen(nombre)+1);

	if(socketcliente.Connect((char *)"127.0.0.1",4800) != 0)
		printf("Error en la conexion del socket\n");
	else
		printf("Exito en la creacion del socket\n");
		
	if(socketcliente.Send(nombre,strlen(nombre)+1) < 0)
		printf("Error en el envio del nombre\n");
	else
		printf("Longitud de la cadena: %d\n",socketcliente.Send(nombre,strlen(nombre)+1));

//		
//	if(socketcrono.Connect((char *)"127.0.0.1",4800) != 0)
//		printf("Error en la conexion del socket\n");
//	else
//		printf("Exito en la creacion del socket\n");
//		
			
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
}

void CMundo::CreaEsferas(void)
{
//	if(nEsferas <= MAX_ESFERAS)
//	{
//		esferas[nEsferas] = new Esfera(20); //El numero da igual, solo es para diferenciar los constructores
//		nEsferas++;
//	}
}

void CMundo::DestruyeEsferas(void)
{
/*	if(nEsferas > 0)
	{
		sprintf(cadfifo, "Esferas destruidas");
	
		if(write(loggerfifo, cadfifo, (strlen(cadfifo)+1)*sizeof(char)) == -1)
		{
			perror("Error en la escritura");
			exit(1);
		}
	}
*/	
//	lamasCercana = &esfera;
	
//	int i;
//	for(i=0; i<nEsferas; i++)
//	{
//		delete esferas[i];
////	}
//	nEsferas = 0;
	
	
}

void CMundo::CreaDisparo(Vector2D centro, float velocidadX, int jugador)
{
/*	switch(jugador)
	{
		case 1: if(disparo1A == false)
				{
					disparo1A = true;
					disparo1 = new Esfera(centro, velocidadX);
					
//					sprintf(cadfifo, "Jugador 1 dispara");
		
//					if(write(loggerfifo, cadfifo, (strlen(cadfifo)+1)*sizeof(char)) == -1)
//					{
//						perror("Error en la escritura");
//						exit(1);
//					}			
				}
				break;
		case 2: if(disparo2A == false)
				{
					disparo2A = true;
					disparo2 = new Esfera(centro, velocidadX);
					
//					sprintf(cadfifo, "Jugador 2 dispara");
		
//					if(write(loggerfifo, cadfifo, (strlen(cadfifo)+1)*sizeof(char)) == -1)
//					{
//						perror("Error en la escritura");
//						exit(1);
//					}	
				}
				break;
		default:break;	
	}*/
}

void CMundo::DesactivaBot(void)
{
	botActivo = 0;
	contador = 0.0f;
}

